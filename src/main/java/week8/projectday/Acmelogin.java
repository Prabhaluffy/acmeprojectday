package week8.projectday;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Acmelogin {
	@Test(dataProvider="fetchdata")
	public void acme(String taxid) throws InterruptedException {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.get("https://acme-test.uipath.com/account/login");
	driver.manage().window().maximize();
	driver.findElementById("email").sendKeys("prabhakarancs2512@gmail.com");
	driver.findElementById("password").sendKeys("nandhana");
	driver.findElementById("buttonLogin").click();
	Thread.sleep(3000);
	WebElement ele = driver.findElementByXPath("//button[text()=' Vendors']");
	Actions builder=new Actions(driver);
	builder.moveToElement(ele).perform();
	driver.findElementByLinkText("Search for Vendor").click();
	Thread.sleep(2000);
	driver.findElementById("vendorTaxID").sendKeys(taxid);
	driver.findElementById("buttonSearch").click();
	WebElement ele2 = driver.findElementByXPath("(//table[@class='table']//td)[1]");
	System.out.println("Vendor name is: " +ele2.getText());
	driver.close();
	}
	@DataProvider(name="fetchdata")
	public Object[] fetch() throws IOException {
		Object[] data=new Object[2];
		data[0]="RO123456";
		data[1]="DE325476";
		return data;
	}
}