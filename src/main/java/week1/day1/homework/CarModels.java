package week1.day1.homework;

public class CarModels {
	public String getModel(String brandname){
	if(brandname.equalsIgnoreCase("BMW"))
	{
		return "BMW X5";
	}else if(brandname.equalsIgnoreCase("Bugatti"))
	{
		return "Bugatti Chiron";
	}else
	{
		return "Enter the correct brandname";
	}
	}
}
