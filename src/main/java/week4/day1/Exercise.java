package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Exercise {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		//switching to window 1
		Set<String> allwin = driver.getWindowHandles();
		List<String> ls=new ArrayList<>();
		ls.addAll(allwin);
		driver.switchTo().window(ls.get(1));
		driver.findElementByXPath("//input[@name='firstName']").sendKeys("Nagato");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		WebElement ele = driver.findElementByXPath("(//table[@class='x-grid3-row-table']//td//a)[1]");
		String ID = ele.getText();
		//System.out.println(ID);
		driver.findElementByXPath("(//table[@class='x-grid3-row-table']//td//a)[1]").click();
		//switching back to 1st window
		driver.switchTo().window(ls.get(0));
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		//switching to window 2
		Set<String> allwin2 = driver.getWindowHandles();
		List<String> ls2=new ArrayList<>();
		ls2.addAll(allwin2);
		driver.switchTo().window(ls2.get(1));
		driver.findElementByXPath("//input[@name='firstName']").sendKeys("Madara");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//table[@class='x-grid3-row-table']//td//a)[1]").click();
		//switching back to Original window
		driver.switchTo().window(ls2.get(0));
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert().accept();
		Thread.sleep(4000);
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//input[@name='id']").sendKeys(ID);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		WebElement ele2 = driver.findElementByXPath("//div[@class='x-paging-info']");
		String eMsg = ele2.getText();
		if(eMsg.equals("No records to display"))
		{
			System.out.println("Records Merged");
		}else
		{
			System.out.println("Records not merged");
		}
		driver.close();
	}
}
