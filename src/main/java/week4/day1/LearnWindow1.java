package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindow1 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElementByLinkText("FLIGHTS").click();
		Set<String> allwin = driver.getWindowHandles();
		List<String> ls=new ArrayList<>();
		ls.addAll(allwin);
		driver.switchTo().window(ls.get(1));
		driver.findElementByXPath("//div[@class='tour-packages']").click();
		System.out.println(driver.getTitle());
	}

}
