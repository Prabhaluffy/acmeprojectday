package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Learnframe {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		String cName="Sathya";
		driver.switchTo().alert().sendKeys(cName);
		driver.switchTo().alert().accept();
		WebElement ele = driver.findElementByXPath("//p[@id='demo']");
		String name = ele.getText();
		System.out.println(name);
		if(name.contains(cName))
		{
			System.out.println("Entered name contains " +cName);
		}else
		{
			System.out.println("Please check the program");
		}
	}
}
