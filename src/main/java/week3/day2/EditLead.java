package week3.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class EditLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Prabhakaran");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		//driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		Thread.sleep(3000);
		driver.findElementByXPath("(//table[@class='x-grid3-row-table']//td//a)[1]").click();
		String title= driver.getTitle();
		System.out.println("The Title of the page is:" + title);
		if(title.contains("View Lead"))
		{
			System.out.println("Title contains View Lead");
		}else
		{
			System.out.println("Title doesn't contain View Lead");
		}	
		WebElement CN=driver.findElementByXPath("//span[@id='viewLead_companyName_sp']");
		String CN1=CN.getText();
		System.out.println("Previous Company Name:"+CN1);
		driver.findElementByLinkText("Edit").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("Accenture");
		driver.findElementByXPath("//input[@name='submitButton']").click();
		WebElement change= driver.findElementByXPath("//span[@id='viewLead_companyName_sp']");
		String CN2=change.getText();
		System.out.println("Updated Company Name:"+CN2);
		if(CN1.equals(CN2))
		{
			System.out.println("Company name not Changed");
		}else
		{
		System.out.println("Company Name updated");
		}
		driver.close();
	}
}
