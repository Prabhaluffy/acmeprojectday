package week3.day2;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnWAIT {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.flipkart.com");
		driver.manage().window().maximize();
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		Actions builder=new Actions(driver);
		WebElement ele = driver.findElementByXPath("//span[text()='TVs & Appliances']");
		builder.moveToElement(ele).perform();
		Thread.sleep(4000);
		driver.findElementByLinkText("Samsung").click();
	}

}
