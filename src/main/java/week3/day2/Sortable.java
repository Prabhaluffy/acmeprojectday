package week3.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Sortable {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/");
		driver.manage().window().maximize();
		driver.findElementByXPath("((//div[@id='post-153']//div)[2]//a)[16]").click();
		WebElement src = driver.findElementByXPath("(//ul[@id='sortable']//li)[1]");
		WebElement tar = driver.findElementByXPath("(//ul[@id='sortable']//li)[7]");
		Actions builder=new Actions(driver);
		int y = driver.findElementById("mydiv").getLocation().getY();
		builder.dragAndDropBy(src, 0, 230).perform();;
		//builder.dragAndDrop(src, tar).perform();
		//builder.clickAndHold(tar);

	}

}
