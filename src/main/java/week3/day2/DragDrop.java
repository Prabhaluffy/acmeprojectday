package week3.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragDrop {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/");
		driver.manage().window().maximize();
		driver.findElementByXPath("((//div[@id='post-153']//div)[2]//a)[14]").click();
		WebElement src = driver.findElementById("draggable");
		WebElement tar = driver.findElementById("droppable");
		Actions builder=new Actions(driver);
		builder.dragAndDrop(src, tar).perform();
		Thread.sleep(3000);
		driver.close();
	}

}
