package week3.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Selectable {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/");
		driver.manage().window().maximize();
		driver.findElementByXPath("((//div[@id='post-153']//div)[2]//a)[15]").click();
		WebElement ele1 = driver.findElementByXPath("(//ol[@id='selectable']//li)[1]");
		WebElement ele2 = driver.findElementByXPath("(//ol[@id='selectable']//li)[2]");
		WebElement ele3 = driver.findElementByXPath("(//ol[@id='selectable']//li)[3]");
		Actions builder=new Actions(driver);
		builder.clickAndHold(ele1).clickAndHold(ele2).clickAndHold(ele3).perform();
		Thread.sleep(3000);
		driver.close();
	}

}
