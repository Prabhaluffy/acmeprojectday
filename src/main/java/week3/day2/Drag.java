package week3.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Drag {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/");
		driver.manage().window().maximize();
		driver.findElementByXPath("//*[@id=\"post-153\"]/div[2]/div/ul/li[13]/a").click();
		Actions builder=new Actions(driver);
		WebElement drag = driver.findElementById("draggable");
		System.out.println(driver.findElementByXPath("//*[@id=\"mydiv\"]").getLocation());
		builder.dragAndDropBy(drag, 298, 0).perform();
		//int x = driver.findElementById("mydiv").getLocation().getX();
		//int y = driver.findElementById("mydiv").getLocation().getY();
		//System.out.println(x);
		//System.out.println(y)
		Thread.sleep(2000);
		builder.dragAndDropBy(drag ,-298,0).perform();
		//WebElement drag2 = driver.findElementById("draggable");
		//builder.dragAndDropBy(drag).perform();
		driver.close();
	}

}
