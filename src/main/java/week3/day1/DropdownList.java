package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropdownList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Accenture");
		driver.findElementById("createLeadForm_firstName").sendKeys("Prabhakaran");
		driver.findElementById("createLeadForm_lastName").sendKeys("Nageswaran");
		WebElement source=driver.findElementById("createLeadForm_dataSourceId");
		Select dds=new Select(source); 
		dds.selectByIndex(2);
		WebElement source1=driver.findElementById("createLeadForm_industryEnumId");
		Select ddi=new Select(source1);
		List<WebElement> opt = ddi.getOptions();
		int size=opt.size();
		ddi.selectByIndex(size-1);
		
		
		//driver.findElementByClassName("smallSubmit").click();
		//driver.close();
	}

}
