package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			ChromeDriver driver=new ChromeDriver();
			driver.get("http://leaftaps.com/opentaps");
			driver.manage().window().maximize();
			driver.findElementById("username").sendKeys("DemoSalesManager");
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByClassName("decorativeSubmit").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Create Lead").click();
			driver.findElementById("createLeadForm_companyName").sendKeys("Sathya");
			driver.findElementById("createLeadForm_firstName").sendKeys("V");
			driver.findElementById("createLeadForm_lastName").sendKeys("S");
			WebElement source = driver.findElementById("createLeadForm_dataSourceId");
			Select dds=new Select(source);
			dds.selectByIndex(2);
			WebElement source1 = driver.findElementById("createLeadForm_industryEnumId");
			Select dd1=new Select(source1);
			List<WebElement> allop = dd1.getOptions();
			int size = allop.size();
			dd1.selectByIndex(size-1);
			WebElement source2 = driver.findElementById("createLeadForm_ownershipEnumId");
			Select dd2=new Select(source2);
			/*List<WebElement> all = dd2.getOptions();
			for (WebElement printall: all) {
				System.out.println(printall.getText());
			}	*/ 
			//dd2.selectByValue("OWN_PROPRIETOR");
			dd2.selectByVisibleText("LLC/LLP");
			
			//driver.findElementByClassName("smallSubmit").click();
			//driver.close();
			
	}


}
