package week3.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IRCTC {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver irctc=new ChromeDriver();
		irctc.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		irctc.manage().window().maximize();
		irctc.findElementById("userRegistrationForm:userName").sendKeys("PrabhaPain");
		irctc.findElementByLinkText("Check Availability").click();
		irctc.findElementById("userRegistrationForm:password").sendKeys("Testleaf1");
		irctc.findElementById("userRegistrationForm:confpasword").sendKeys("Testleaf1");
		WebElement dd1=irctc.findElementById("userRegistrationForm:securityQ");
		Select irc=new Select(dd1);
		irc.selectByIndex(2);
		irctc.findElementById("userRegistrationForm:securityAnswer").sendKeys("HolyCross");
		irctc.findElementById("userRegistrationForm:firstName").sendKeys("Prabhakaran");
		irctc.findElementById("userRegistrationForm:gender:0").click();
		irctc.findElementById("userRegistrationForm:maritalStatus:0").click();
		irctc.findElementById("userRegistrationForm:dobDay").sendKeys("25");
		irctc.findElementById("userRegistrationForm:dobMonth").sendKeys("DEC");
		irctc.findElementById("userRegistrationForm:dateOfBirth").sendKeys("1992");
		WebElement dd2=irctc.findElementById("userRegistrationForm:occupation");
		Select occp=new Select(dd2);
		occp.selectByVisibleText("Professional");
		WebElement dd3=irctc.findElementById("userRegistrationForm:countries");
		Select coun=new Select(dd3);
		coun.selectByIndex(1);
		irctc.findElementById("userRegistrationForm:email").sendKeys("prabhakarancs2512@gmail.com");
		irctc.findElementById("userRegistrationForm:mobile").sendKeys("9994625897",Keys.TAB);
	    irctc.findElementById("userRegistrationForm:nationalityId").sendKeys("India");
		irctc.findElementById("userRegistrationForm:address").sendKeys("FLat NO:F1/1st floor");
		irctc.findElementById("userRegistrationForm:pincode").sendKeys("600063", Keys.TAB);
		//irctc.findElementById("userRegistrationForm:cityName").sendKeys("Kanchipuram");
		//irctc.findElementById("userRegistrationForm:postofficeName").sendKeys(" ");
		irctc.findElementById("userRegistrationForm:landline").sendKeys("04466265351");
		//irctc.close();
	}

}
