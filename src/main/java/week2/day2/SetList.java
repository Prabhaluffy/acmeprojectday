package week2.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SetList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Set<String> allmod=new TreeSet<>();
		System.out.println("Initial Size: " +allmod.size());
		allmod.add("Apple");
		allmod.add("Samsung");
		allmod.add("One Plus");
		allmod.add("Honor");
		for (String eachmodel : allmod) {
			System.out.println(eachmodel);
		}
		System.out.println("Total no.of Models after adding: " +allmod.size());
		allmod.remove("Samsung");
		System.out.println("After Removing the last Model:");
		for (String aftrem : allmod) {
			System.out.println(aftrem);
		}
		List<String> mod=new ArrayList<String>(allmod);
		System.out.println("2nd Model is the Set is: " +mod.get(1));
		allmod.clear();
		System.out.println(allmod.isEmpty());
	}

}
