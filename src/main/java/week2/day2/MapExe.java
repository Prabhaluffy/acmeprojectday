package week2.day2;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class MapExe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int max=0;
		Map<String,Integer> map=new TreeMap<>();
		map.put("Puma", 10);
		map.put("Reebok", 30);
		map.put("Addidas", 35);
		map.put("Bata", 55);
		map.put("Action", 456);
		for (Entry<String, Integer> str : map.entrySet())
		{
			if(max<str.getValue())
			{
				max=str.getValue();
			}
		}
		for (Entry<String, Integer> st : map.entrySet()) {
			if(max==st.getValue())
			{
				System.out.println(st.getKey());
			}
		}
		System.out.println(max);
	}

}
