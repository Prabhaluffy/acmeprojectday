package week2.day2;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Maplearn {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the data");
		String data = sc.nextLine();
		Map<Character,Integer> map=new LinkedHashMap<>();
		char[] ch=data.toCharArray();
		//System.out.println(ch);
		for (char c : ch) {
			if(map.containsKey(c)) {
				map.put(c,map.get(c)+1);
			}else
			map.put(c, 1);
		}
		System.out.println(map);
		sc.close();
	}
}
