package week2.day1.exercise;

public interface Vehicle {

	public void getColor();
	public int getPrice();
	public String getBrand();
}
