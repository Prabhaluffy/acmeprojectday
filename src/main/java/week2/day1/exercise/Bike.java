package week2.day1.exercise;

public class Bike extends PasVehicle {

	public void getModel() {
		System.out.println("Bike Model is Ducati V3000");
	}

	@Override
	public void getColor() {
		// TODO Auto-generated method stub
		System.out.println("RED");
	}

	@Override
	public int getPrice() {
		// TODO Auto-generated method stub
		return 15000;
	}

	@Override
	public String getBrand() {
		// TODO Auto-generated method stub
		return "Ducati";
	}
	
}
